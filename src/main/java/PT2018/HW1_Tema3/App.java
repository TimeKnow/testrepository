package PT2018.HW1_Tema3;


import PT2018.HW1_Tema3.company.presentation.View;
import PT2018.HW1_Tema3.company.presentation.ViewController;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        View view = new View ();
        view.setVisible(true);
        view.draw();
        ViewController viewController = new ViewController(view);
    }
}
