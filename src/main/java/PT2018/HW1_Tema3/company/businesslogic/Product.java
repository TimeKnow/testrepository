package PT2018.HW1_Tema3.company.businesslogic;

public class Product {
    private int id;
    private String name;
    private float value;
    private int Id_Supplier;
    private String description;
    private int quantity;

    public Product() {
    }

    public Product(int id, String name, float value, int idSupplier, String description) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.Id_Supplier = idSupplier;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getId_Supplier() {
        return Id_Supplier;
    }

    public void setId_Supplier(int id_Supplier) {
        this.Id_Supplier = id_Supplier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", Id_Supplier=" + Id_Supplier +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
