package PT2018.HW1_Tema3.company.presentation;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class View extends JFrame{
    private static final int HEIGHT=280;
    private static final int WIDTH=700;
    private JPanel mainPanel;
    private ArrayList<JButton> buttonList = new ArrayList<>();
    private static final String[] buttonNames = new String[]{"Products","Clients","Orders","Suppliers"};


    public View(){

    }

    public void draw(){
        buttonList.clear();
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new FlowLayout());
        mainPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setMaximumSize(new Dimension(this.WIDTH/3,this.HEIGHT));
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.Y_AXIS));
        buttonPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        for(int i=0;i<4;i++){
            JButton button = new JButton();
            button.setHorizontalAlignment(JButton.CENTER);
            button.setMinimumSize(new Dimension(this.WIDTH/2,50));
            button.setPreferredSize(new Dimension(this.WIDTH/2,50));
            button.setText(buttonNames[i]);
            button.setAlignmentX(JButton.CENTER_ALIGNMENT);
            button.setAlignmentY(JButton.CENTER);


            JPanel jPanel = new JPanel();
            jPanel.setLayout(new FlowLayout());
            jPanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
            jPanel.setPreferredSize(new Dimension(this.WIDTH/2,50));
            jPanel.add(button);
            buttonList.add(button);
            buttonPanel.add(jPanel);

        }

        mainPanel.add(buttonPanel);
        this.setContentPane(this.mainPanel);
        this.setTitle("Warehouse Management");
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo((Component)null);
        this.setDefaultCloseOperation(3);
    }

    public ArrayList<JButton> getButtonList() {
        return buttonList;
    }
}
