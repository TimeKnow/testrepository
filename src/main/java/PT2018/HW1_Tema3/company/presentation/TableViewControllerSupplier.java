package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.businesslogic.Supplier;
import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;
import PT2018.HW1_Tema3.company.dataaccess.SupplierDAO;

import java.util.List;

public class TableViewControllerSupplier extends TableViewController<Supplier> {

    public TableViewControllerSupplier() {
        this.setAbstractDAO(new SupplierDAO());
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void refresh() {
        int currentRowCount = getTableView().getModel().getRowCount();
        for(int i=currentRowCount-1;i>=0;i--)
            this.getTableView().getModel().removeRow(i);

        List<Supplier> suppliers = this.getAbstractDAO().findAll();
        for(Supplier supplier : suppliers){
            List<Object[]> dataProduct = AbstractDAO.retrieveProperties(supplier);
            Object[] values = new Object[dataProduct.size()];
            int k=0;
            for(Object[] o : dataProduct)
                values[k++]=o[1];
            this.getTableView().getModel().addRow(values);
        }
        this.getTableView().getModel().addRow(new Object[getTableView().getColumnsLength()]);

    }

    @Override
    public int insert(Object data) {
        return 0;
    }



}
