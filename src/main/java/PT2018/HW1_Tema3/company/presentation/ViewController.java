package PT2018.HW1_Tema3.company.presentation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewController implements ActionListener {

    private View view;

    public ViewController(View view) {
        this.view = view;
        for(JButton b : view.getButtonList())
            b.addActionListener(this);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==view.getButtonList().get(0))
            productEvent();
        if(e.getSource()==view.getButtonList().get(1))
            clientEvent();
        if(e.getSource()==view.getButtonList().get(2))
            ordersEvent();
        if(e.getSource()==view.getButtonList().get(3))
            supplierEvent();
    }
    @SuppressWarnings("Duplicates")
    public void productEvent(){
        TableViewProduct productTableView = new TableViewProduct();
        productTableView.setVisible(true);
        productTableView.draw();
        TableViewControllerProduct tableViewControllerProduct = new TableViewControllerProduct();
        for(JButton b : productTableView.getButtonList())
            b.addActionListener(tableViewControllerProduct);
        tableViewControllerProduct.setTableView(productTableView);
        tableViewControllerProduct.refresh();

    }
    @SuppressWarnings("Duplicates")
    public void ordersEvent(){
        TableViewOrders viewOrders = new TableViewOrders();
        viewOrders.setVisible(true);
        viewOrders.draw();
        TableViewControllerOrders tableViewOrders = new TableViewControllerOrders();
        for(JButton b : viewOrders.getButtonList())
            b.addActionListener(tableViewOrders);
        tableViewOrders.setTableView(viewOrders);
        tableViewOrders.refresh();
    }
    @SuppressWarnings("Duplicates")
    public void clientEvent(){
        TableViewClient viewClient = new TableViewClient();
        viewClient.setVisible(true);
        viewClient.draw();
        TableViewControllerClient tableViewControllerClient = new TableViewControllerClient();
        for(JButton b : viewClient.getButtonList())
            b.addActionListener(tableViewControllerClient);
        tableViewControllerClient.setTableView(viewClient);
        tableViewControllerClient.refresh();
    }
    @SuppressWarnings("Duplicates")
    public void supplierEvent(){

        TableViewSupplier viewSupplier = new TableViewSupplier();
        viewSupplier.setVisible(true);
        viewSupplier.draw();
        TableViewControllerSupplier tableViewControllerSupplier = new TableViewControllerSupplier();
        for(JButton b : viewSupplier.getButtonList())
            b.addActionListener(tableViewControllerSupplier);
        tableViewControllerSupplier.setTableView(viewSupplier);
        tableViewControllerSupplier.refresh();
    }
}
