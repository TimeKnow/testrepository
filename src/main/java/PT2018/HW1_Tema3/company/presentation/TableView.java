package PT2018.HW1_Tema3.company.presentation;

import PT2018.HW1_Tema3.company.dataaccess.AbstractDAO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class TableView<E> extends JFrame {
    private static final int HEIGHT=500;
    private static final int WIDTH=900;
    private JPanel mainPanel;
    private JTable table;
    private JScrollPane scrollPane;
    private DefaultTableModel model;
    private ArrayList<JButton> buttonList = new ArrayList<>();
    private static final String[] buttonNames = new String[]{"Insert","Delete","Update","Refresh"};
    private int columnsLength;

    private final Class<E> type;

    @SuppressWarnings("unchecked")
    public TableView(){
        this.type = (Class<E>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void draw(){
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new FlowLayout());
        mainPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        try {
            JPanel tablePanel = new JPanel();
            tablePanel.setLayout(new FlowLayout());
            tablePanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

            List<Object[]> dataType = AbstractDAO.retrieveProperties(type.newInstance());
            String[] columnNames = new String[dataType.size()];
            for (int i = 0; i < dataType.size(); i++)
                columnNames[i] = String.valueOf(dataType.get(i)[0]);
            columnsLength = columnNames.length;
            model = new DefaultTableModel(0, columnNames.length) ;
            model.setColumnIdentifiers(columnNames);
            
            table = new JTable(model);

            table.setPreferredSize(new Dimension(((int)(2.8f*WIDTH))/4,3*HEIGHT/4));
            table.setAlignmentX(SwingConstants.LEFT);
            scrollPane = new JScrollPane(table);
            scrollPane.setPreferredSize(new Dimension(((int)(2.8f*WIDTH))/4,3*HEIGHT/4));
            tablePanel.add(scrollPane);
            tablePanel.setPreferredSize(new Dimension(((int)(2.8f*WIDTH))/4,3*HEIGHT/4));

            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.Y_AXIS));
            for(int i=0;i<buttonNames.length;i++){
                JButton button = new JButton();
                button.setHorizontalAlignment(JButton.CENTER);
                button.setMinimumSize(new Dimension(this.WIDTH/4,40));
                button.setPreferredSize(new Dimension(this.WIDTH/4,40));
                button.setText(buttonNames[i]);
                button.setAlignmentX(JButton.CENTER_ALIGNMENT);
                button.setAlignmentY(JButton.CENTER);


                JPanel jPanel = new JPanel();
                jPanel.setLayout(new FlowLayout());
                jPanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
                jPanel.setPreferredSize(new Dimension(this.WIDTH/4,40));
                jPanel.add(button);
                buttonList.add(button);
                buttonPanel.add(jPanel);

            }
            mainPanel.add(buttonPanel);
            mainPanel.add(tablePanel);

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        this.setContentPane(this.mainPanel);
        this.setTitle("Warehouse Management");
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo((Component)null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE );
    }

    public JTable getTable() {
        return table;
    }

    public DefaultTableModel getModel() {
        return model;
    }

    public ArrayList<JButton> getButtonList() {
        return buttonList;
    }

    public int getColumnsLength() {
        return columnsLength;
    }
}
