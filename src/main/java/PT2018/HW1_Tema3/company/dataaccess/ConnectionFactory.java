package PT2018.HW1_Tema3.company.dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectionFactory {
    private static final String DRIVER = "jdbc:mysql://localhost:3306/warehousemanagement";//jdbc:mysql://localhost:3306/warehousemanagement
    private static final String USER = "warehouseManager";
    private static final String PASSWORD = "warehouseManager";

    private static ConnectionFactory instance = new ConnectionFactory();

    private ConnectionFactory(){

    }

    private Connection createConnection(){
        Connection connection=null;
        try {
             connection = DriverManager.getConnection(DRIVER, USER, PASSWORD);
            //System.out.println("Connection made!");
            return connection;
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Connection failed!");
            return null;
        }
    }

    public static Connection getConnection(){
        return instance.createConnection();
    }

    public static void close(Connection connection){
        if(connection!=null) {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Connection failed to close!");
            }
        }
    }

    public static void close(Statement statement){
        if(statement!=null) {
            try {
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Statement failed to close!");
            }
        }
    }

    public static void close(ResultSet resultSet){
        if(resultSet!=null) {
            try {
                resultSet.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ResultSet failed to close!");
            }
        }
    }
}
